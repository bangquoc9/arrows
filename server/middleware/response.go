package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"

	archiveErr "gitlab.com/leducthai/archive/error"
)

type ResponseErrorDetails struct {
	ResponseDetails archiveErr.Error
}

type ResponseOK struct {
	ResponseDetails any
}

type Response error

func ResponseErrorFunc(ctx *gin.Context, httpCode int, errDetails ResponseErrorDetails) Response {
	ctx.JSON(httpCode, errDetails.ResponseDetails)
	return nil
}

func ResponseOKFunc(ctx *gin.Context, content any) Response {
	ctx.JSON(http.StatusOK, content)
	return nil
}
