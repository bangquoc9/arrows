package config

import "time"

type Options struct {
	GatewayHost string `long:"gateway-host" description:"the IP to listen on" default:"localhost" env:"ARROWS_GATEWAY_HOST"`
	GatewayPort int    `long:"gateway-port" description:"gateway port to bind" default:"8081" env:"ARROWS_GATEWAY_PORT"`

	// development logging mode
	DevMode bool `long:"dev-mode" description:"development mode" env:"ARROWS_DEV_MODE"`

	// each request timeout, default value is 1 minute
	Timeout time.Duration `long:"timeout" description:"request timeout, need to fetch unit time" default:"1m" env:"ARROWS_TIMEOUT"`

	// server settings
	ConfigPath string `long:"config" description:"server settings path" env:"ARROWS_CONFIG"`
}

type TLSOptions struct {
	TLSCert string `long:"tls-cert" description:"path to TLS certificate (PUBLIC). To enable TLS handshake, you must set this value" env:"ARROWS_TLS_CERT"`
	TLSKey  string `long:"tls-key" description:"path to TLS certificate key (PRIVATE), To enable TLS handshake, you must set this value" env:"ARROWS_TLS_KEY"`
}

func (t *TLSOptions) UseTLS() bool {
	return t.TLSCert != "" && t.TLSKey != ""
}

type Config struct {
	PostGres *PostGresInfo `yaml:"postgres"`
}

type PostGresInfo struct {
	Schema   string `yaml:"schema"`
	Name     string `yaml:"name"`
	Address  string `yaml:"address"`
	Port     int    `yaml:"port"`
	UserName string `yaml:"userName"`
	Password string `yaml:"password"`
}
