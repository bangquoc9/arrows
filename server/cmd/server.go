package cmd

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-openapi/swag"
	"github.com/jessevdk/go-flags"
	"go.uber.org/zap"
	"gopkg.in/yaml.v3"

	"gitlab.com/leducthai/archive/impl/services/dm"

	"gitlab.com/quocbang/arrows/config"
	"gitlab.com/quocbang/arrows/impl"
	"gitlab.com/quocbang/arrows/middleware"
	"gitlab.com/quocbang/arrows/routes"
)

// Config is config option of flags.
type Config struct {
	Options    config.Options
	TLSOptions config.TLSOptions
}

func parseFlags() *Config {
	var conf Config

	configurations := []swag.CommandLineOptionsGroup{
		{
			ShortDescription: "Server Configuration",
			LongDescription:  "Server Configuration",
			Options:          &conf.Options,
		},
		{
			ShortDescription: "TLS handshake Configuration",
			LongDescription:  "TLS handshake Configuration",
			Options:          &conf.TLSOptions,
		},
	}

	parser := flags.NewParser(nil, flags.Default)
	for _, optGroup := range configurations {
		if _, err := parser.AddGroup(optGroup.ShortDescription, optGroup.LongDescription, optGroup.Options); err != nil {
			log.Fatalln(err)
		}
	}

	if _, err := parser.Parse(); err != nil {
		code := 1
		if fe, ok := err.(*flags.Error); ok && fe.Type == flags.ErrHelp {
			code = 0
		}
		os.Exit(code)
	}

	return &conf
}

func loadConfig(configPath string) (*config.Config, error) {
	var conf config.Config

	// read file by path.
	data, err := os.ReadFile(configPath)
	if err != nil {
		return nil, err
	}

	// parse the information to struct.
	if err := yaml.Unmarshal(data, &conf); err != nil {
		return nil, err
	}

	return &conf, nil
}

func RunServer() {
	server := gin.New()

	// config flags.
	flags := parseFlags()

	// parse config.
	configs, err := loadConfig(flags.Options.ConfigPath)
	if err != nil {
		log.Fatalf("load config failed, error: %v", err)
	}

	// Initialize the Zap logger
	var logger *zap.Logger
	if !flags.Options.DevMode {
		logger, err = zap.NewProduction()
	} else {
		logger, err = zap.NewDevelopment()
	}
	if err != nil {
		log.Fatalf("failed to initialize logger error: %v", err.Error())
	}
	defer logger.Sync()

	// Use the custom logging middleware
	server.Use(middleware.LoggingMiddleware(logger))

	// register data manager.
	opts := []dm.Option{
		dm.WithPostgreSQLSchema(configs.PostGres.Schema),
		dm.AutoMigrateTables(),
	}
	ctx := &gin.Context{}
	dataManager, err := dm.New(ctx, dm.PGConfig{
		Address:  configs.PostGres.Address,
		Port:     configs.PostGres.Port,
		UserName: configs.PostGres.UserName,
		Password: configs.PostGres.Password,
		Database: configs.PostGres.Name,
	}, opts...)
	if err != nil {
		log.Fatal(err)
	}

	// register service.
	serviceConfig := impl.ServiceConfig{
		TokenLifeTime: time.Hour * 8,
		Printers:      nil, // TODO: check printers name in config file.
	}
	s, err := impl.RegisterService(dataManager, serviceConfig)
	if err != nil {
		log.Fatal(err)
	}

	// register routes and redirect to handlers.
	routes.RegisterRoutesService(&server.RouterGroup, s)

	// setup run address.
	addr := fmt.Sprintf("%s:%d", flags.Options.GatewayHost, flags.Options.GatewayPort)

	// run with TLS.
	if flags.TLSOptions.UseTLS() {
		server.RunTLS(addr, flags.TLSOptions.TLSCert, flags.TLSOptions.TLSKey)
	} else { // run without TLS
		server.Run(addr)
	}
}
