package validate

import (
	"fmt"
	"reflect"
	"testing"

	fake "github.com/brianvoe/gofakeit"
	"github.com/stretchr/testify/assert"
)

type validateTestStruct struct {
	ID   string    `validate:"required"`
	Name string    `validate:"required"`
	Info userInfor `validate:"required,dive,omitempty"`
}

type userInfor struct {
	PhoneNumber int    // no rule
	Age         int    `validate:"min=18"`
	Address     string `validate:"required,min=20"`
}

func TestValidator(t *testing.T) {
	assertion := assert.New(t)

	goodCase := validateTestStruct{
		ID:   fake.Company(),
		Name: fake.Name(),
		Info: userInfor{
			PhoneNumber: fake.CreditCardNumberLuhn(),
			Age:         20,
			Address:     fake.Address().Address,
		},
	}

	// with one bad case but it has multiple unexpected case in there.
	badCase := []validateTestStruct{
		{ // missing ID
			Name: fake.Company(),
			Info: userInfor{
				PhoneNumber: fake.CreditCard().Number,
				Age:         20,
				Address:     fake.Address().Address,
			},
		},
		{ // missing name.
			ID: fake.Company(),
			Info: userInfor{
				PhoneNumber: fake.CreditCardNumber(),
				Age:         20,
				Address:     fake.Address().Address,
			},
		},
		{ // missing address.
			ID:   fake.Name(),
			Name: fake.Name(),
			Info: userInfor{
				PhoneNumber: fake.CreditCardNumber(),
				Age:         20,
			},
		},
		{ // the characters of the address field are not sufficient.
			ID:   fake.Name(),
			Name: fake.Name(),
			Info: userInfor{
				PhoneNumber: fake.CreditCardNumber(),
				Age:         18,
				Address:     "short than 20 chars",
			},
		},
	}

	// good case.
	{
		assertion.NoError(ValidateStruct[validateTestStruct](goodCase))
	}

	// bad cases.
	{
		for _, v := range badCase {
			err := ValidateStruct[validateTestStruct](v)
			assertion.Error(err)
		}
	}
	{ // type are not allow.
		// arrange
		validateTestStructPointer := validateTestStruct{
			ID:   fake.Name(),
			Name: fake.Extension(),
			Info: userInfor{
				PhoneNumber: fake.CreditCardNumber(),
				Address:     fake.Address().Address,
			},
		}

		// act
		actual := ValidateStruct[*validateTestStruct](&validateTestStructPointer)

		// assertion
		expected := fmt.Errorf("type %v, %v", reflect.TypeOf(&validateTestStructPointer).Kind(), TypeNotAllow)
		assertion.Equal(expected, actual)
	}
}
