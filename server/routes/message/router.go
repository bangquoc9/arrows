package message

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/quocbang/arrows/impl/handlers/message"
)

func Message(messageRoutes *gin.RouterGroup) {
	{
		messageRoutes.GET("/connection/:ID", func(ctx *gin.Context) {
			message.ListMessageConnection(ctx)
		})
	}
}
