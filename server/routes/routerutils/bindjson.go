package routerutils

import (
	"github.com/gin-gonic/gin"
)

func BindJSON[T any](ctx *gin.Context) (T, error) {
	var parameter T
	// using BindJson method to serialize body with struct
	if err := ctx.BindJSON(&parameter); err != nil {
		return parameter, err
	}
	return parameter, nil
}
