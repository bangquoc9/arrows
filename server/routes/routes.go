package routes

import (
	"fmt"

	"github.com/gin-gonic/gin"

	"gitlab.com/quocbang/arrows/impl/service"
	"gitlab.com/quocbang/arrows/routes/auth"
	"gitlab.com/quocbang/arrows/routes/message"
)

const (
	API = "/api"
)

func RegisterRoutesService(router *gin.RouterGroup, service *service.Service) {
	// Authentication Service.
	auth.Authentication(router.Group(fmt.Sprintf("%v/user", API)), service.Auth)

	// Message Service.
	message.Message(router.Group(fmt.Sprintf("%v/message", API)))
}
