package auth

import (
	"net/http"

	"github.com/gin-gonic/gin"

	requests "gitlab.com/quocbang/arrows/impl/requests/request_auth"
	"gitlab.com/quocbang/arrows/impl/service"
	"gitlab.com/quocbang/arrows/routes/routerutils"
)

func Authentication(authRoutes *gin.RouterGroup, s service.Authentication) {
	// login router.
	{
		authRoutes.POST("/login", func(ctx *gin.Context) {
			params, err := routerutils.BindJSON[requests.LoginReqest](ctx)
			if err != nil {
				ctx.AbortWithError(http.StatusBadRequest, err)
				return
			}

			// call to handler area.
			s.Login(ctx, params)
		})
	}
}
