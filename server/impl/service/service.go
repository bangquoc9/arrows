package service

import (
	"github.com/gin-gonic/gin"

	requests "gitlab.com/quocbang/arrows/impl/requests/request_auth"
	"gitlab.com/quocbang/arrows/middleware"
)

type Service struct {
	Auth Authentication
}

type Authentication interface {
	Login(*gin.Context, requests.LoginReqest) middleware.Response
}

func NewService(a Authentication) *Service {
	return &Service{
		Auth: a,
	}
}

func (s *Service) Authentication() Authentication {
	return s.Auth
}
