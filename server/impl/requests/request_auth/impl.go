package requests

type LoginReqest struct {
	UserName string `validate:"required"`
	Password string `validate:"required"`
}

type LoginReply struct {
	APIKey string
}
