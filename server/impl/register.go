package impl

import (
	"time"

	"gitlab.com/leducthai/archive"

	"gitlab.com/quocbang/arrows/impl/handlers/auth"
	"gitlab.com/quocbang/arrows/impl/service"
)

type ServiceConfig struct {
	TokenLifeTime time.Duration
	Printers      map[string]string
}

func RegisterService(dm archive.DataManagerServices, config ServiceConfig) (*service.Service, error) {
	return service.NewService(
		auth.NewAuthorization(dm),
	), nil
}
