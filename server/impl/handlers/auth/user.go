package auth

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"

	"gitlab.com/leducthai/archive"
	archiveError "gitlab.com/leducthai/archive/error"

	requests "gitlab.com/quocbang/arrows/impl/requests/request_auth"
	"gitlab.com/quocbang/arrows/impl/service"
	"gitlab.com/quocbang/arrows/middleware"
	"gitlab.com/quocbang/arrows/utils/validate"
)

type Authorization struct {
	dm archive.DataManagerServices
}

func NewAuthorization(dm archive.DataManagerServices) service.Authentication {
	return Authorization{dm: dm}
}

func (a Authorization) Login(ctx *gin.Context, req requests.LoginReqest) middleware.Response {
	if err := validate.ValidateStruct[requests.LoginReqest](req); err != nil {
		return middleware.ResponseErrorFunc(ctx, http.StatusBadRequest, middleware.ResponseErrorDetails{
			ResponseDetails: archiveError.Error{
				Details: err.Error(),
			},
		})
	}

	return middleware.ResponseOKFunc(ctx, requests.LoginReply{
		APIKey: uuid.New().String(),
	})
}
